---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Troubleshooting the GitLab extension

## Enable debug logging

Both the VS Code Extension and the GitLab Language Server provide logs that can help you troubleshoot. To enable debug logging:

1. In VS Code, on the top bar, go to **Code > Preferences > Settings**.
1. In the top right corner, select **Open Settings (JSON)** to edit your `settings.json` file.
1. Add these lines, or edit them if they already exist:

   ```json
   "gitlab.debug": true,
   "gitlab.ls.debug": true,
   ```

1. Save your changes.

### View log files

To view debug logs from either VS Code Extension or the GitLab Language Server:

1. Use the command `GitLab: Show Extension Logs` to view the output panel.
1. In the upper right corner of the output panel, select either **GitLab Workflow** or **GitLab Language Server** from the dropdown list.

## `407 Access Denied` failure when using a proxy

You might encounter an error similar to `407 Access Denied (authentication_failed)`
if you're using an authenticated proxy:

```plaintext
Request failed: Can't add GitLab account for https://gitlab.com. Check your instance URL and network connection.
Fetching resource from https://gitlab.com/api/v4/personal_access_tokens/self failed
```

GitLab Duo Code Suggestions does not support authenticated proxies. For the proposed feature,
see [issue 1234](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1234).

## Settings for self-signed certificates

To use self-signed certificates to connect to your GitLab instance, configure them using the following settings. These are community contributed because the GitLab team uses a public CA.

These settings don't work with [`http.proxy` setting for VS Code](https://code.visualstudio.com/docs/setup/network#_legacy-proxy-server-support) (see [open issue](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/314)).

**`gitlab.ca`** _(required: false, default: null)_

_Deprecated. Please see [the SSL setup guide](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/docs/user/ssl.md) for more information on how to set up your self-signed CA._

**`gitlab.cert`** _(required: false, default: null)_

**Unsupported** - See [&6244](https://gitlab.com/groups/gitlab-org/-/epics/6244). _If your self-managed GitLab instance requires a custom cert/key pair you would probably need to set this option in to point your certificate file. Please also see `gitlab.certKey` option._

**`gitlab.certKey`** _(required: false, default: null)_

**Unsupported** - See [&6244](https://gitlab.com/groups/gitlab-org/-/epics/6244). _If your self-managed GitLab instance requires a custom cert/key pair you would probably need to set this option in to point your certificate key file. Please also see `gitlab.cert` option._

**`gitlab.ignoreCertificateErrors`** _(required: false, default: false)_

**Unsupported** - See [&6244](https://gitlab.com/groups/gitlab-org/-/epics/6244). _If you are using a self-managed GitLab instance with no SSL certificate or having certificate issues and unable to use the extension you may want to set this option to `true` to ignore certificate errors._

## Disable code suggestions

By default, code completion is enabled and usable as long as you
[have access to the Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#start-using-code-suggestions).

If you would like to disable code completion:

1. On the left sidebar, select **Extensions > GitLab Workflow**.
1. Click the **Manage** cogwheel icon (next to "Uninstall" button), and then select **Extension Settings**.
1. In **GitLab > AI Assisted Code Suggestions**, disable the **Enable GitLab Duo Code Suggestions** checkbox.

## Disable streaming

By default, code generation streams AI-generated code. When streaming is
enabled, the suggested code is generated and sent to your editor incrementally rather than
waiting for the full code snippet to be complete before sending the suggestions.
This allows for a more interactive and responsive experience.

If you prefer to only see code generation results printed in their entirety, you can turn off streaming.
Disabling streaming means that code generation requests may be _perceived_
as taking longer to resolve. To disable streaming:

1. In VS Code, on the top bar, go to **Code > Preferences > Settings**.
1. In the top right corner, select **Open Settings (JSON)** to edit your `settings.json` file.
1. Add this line, or edit it if it already exists, to disable code suggestions streaming:

   ```json
   "gitlab.featureFlags.streamCodeGenerations": false,
   ```

1. Save your changes.

## Error: Direct connection fails

The [GitLab Duo Code Suggestions Direct Connection mechanism](https://gitlab.com/groups/gitlab-org/-/epics/13252) was introduced in GitLab 17.2.

To reduce latency, the Workflow extension tries to send suggestion completion requests directly to GitLab Cloud Connector, bypassing the GitLab instance. This network connection does not use the proxy and certificate settings of the VS Code extension.

If your GitLab instance doesn't support direct connections, or your network prevents the extension from connecting to GitLab Cloud Connector, you might see these warnings in your logs:

```plaintext
Failed to fetch direct connection details from GitLab instance.
Code suggestion requests will be sent to GitLab instance.
```

This error means your instance either doesn't support direct connections, or is misconfigured.
To fix the problem, see the [troubleshooting guide for GitLab instance](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html).

```plaintext
Direct connection for code suggestions failed.
Code suggestion requests will be sent to your GitLab instance.
```

This error means your extension can't connect to GitLab Cloud Connector, and is reverting to use your GitLab instance.
The indirect connection through your GitLab instance is about 100 ms slower, but otherwise works the same.
This issue is often caused by network connection problems, like with your LAN firewall or proxy settings.
