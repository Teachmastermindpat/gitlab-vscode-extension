import { NodeModule, LanguageClient, TransportKind } from 'vscode-languageclient/node';
import {
  LANGUAGE_SERVER_ID,
  LANGUAGE_SERVER_NAME,
  LanguageClientFactory,
} from '../../common/language_server/client_factory';
import { getExtensionConfiguration } from '../../common/utils/extension_configuration';

export const desktopLanguageClientFactory: LanguageClientFactory = {
  createLanguageClient(context, clientOptions) {
    const exec: NodeModule = {
      module: context.asAbsolutePath('./assets/language-server/node/main-bundle.js'),
      transport: TransportKind.stdio,
    };

    const debugArgs = getExtensionConfiguration().languageServerDebug ? ['--inspect=6010'] : []; // this opens debugger connection in the LS
    const runArgs = getExtensionConfiguration().debug ? ['--use-source-maps'] : []; // this initializes source maps for stack trace

    return new LanguageClient(
      LANGUAGE_SERVER_ID,
      LANGUAGE_SERVER_NAME,
      {
        debug: {
          ...exec,
          args: runArgs,
          options: {
            execArgv: ['--nolazy', ...debugArgs],
          },
        },
        run: {
          ...exec,
          args: runArgs,
        },
      },
      clientOptions,
    );
  },
};
