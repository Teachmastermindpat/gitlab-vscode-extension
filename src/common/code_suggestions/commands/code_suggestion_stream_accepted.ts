import { BaseLanguageClient } from 'vscode-languageclient';
import {
  TELEMETRY_NOTIFICATION,
  TRACKING_EVENTS,
  CODE_SUGGESTIONS_CATEGORY,
} from '@gitlab-org/gitlab-lsp';
import { log } from '../../log';
import { CompletionStream } from '../../language_server/completion_stream';

export const CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND = 'gl.codeSuggestionStreamAccepted';
// Used for telemetry
export const codeSuggestionStreamAccepted =
  (client: BaseLanguageClient) => async (stream: CompletionStream) => {
    log.debug(`stream has been accepted ${stream}`);
    stream.cancel();

    await client?.sendNotification(TELEMETRY_NOTIFICATION, {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: TRACKING_EVENTS.ACCEPTED,
      context: { trackingId: stream.trackingId },
    });
  };
