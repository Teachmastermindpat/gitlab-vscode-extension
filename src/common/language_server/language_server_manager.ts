import {
  DidChangeDocumentInActiveEditor,
  SUGGESTION_ACCEPTED_COMMAND,
} from '@gitlab-org/gitlab-lsp';
import vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import { CodeSuggestionsGutterIcon } from '../code_suggestions/code_suggestions_gutter_icon';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import { CodeSuggestionsStatusBarItem } from '../code_suggestions/code_suggestions_status_bar_item';
import {
  CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
  codeSuggestionStreamAccepted,
} from '../code_suggestions/commands/code_suggestion_stream_accepted';
import {
  COMMAND_TOGGLE_CODE_SUGGESTIONS,
  toggleCodeSuggestions,
} from '../code_suggestions/commands/toggle';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { CONFIG_NAMESPACE } from '../constants';
import { DependencyContainer } from '../dependency_container';
import { LanguageClientFactory } from './client_factory';
import { getClientContext } from './get_client_context';
import { LanguageClientMiddleware } from './language_client_middleware';
import { LanguageClientWrapper } from './language_client_wrapper';
import { log } from '../log';

export class LanguageServerManager {
  #client: BaseLanguageClient | undefined;

  #wrapper: LanguageClientWrapper | undefined;

  #context: vscode.ExtensionContext;

  #dependencyContainer: DependencyContainer;

  #clientFactory: LanguageClientFactory;

  #subscriptions: vscode.Disposable[] = [];

  constructor(
    context: vscode.ExtensionContext,
    clientFactory: LanguageClientFactory,
    dependencyContainer: DependencyContainer,
  ) {
    this.#context = context;
    this.#clientFactory = clientFactory;
    this.#dependencyContainer = dependencyContainer;
  }

  public async startLanguageServer() {
    if (this.#client) {
      log.warn('Language server already started');
      return;
    }
    const { gitLabPlatformManager, gitLabTelemetryEnvironment } = this.#dependencyContainer;
    const stateManager = new CodeSuggestionsStateManager(gitLabPlatformManager, this.#context);
    const statusBarItem = new CodeSuggestionsStatusBarItem(stateManager);
    const gutterIcon = new CodeSuggestionsGutterIcon(this.#context, stateManager);
    const middleware = new LanguageClientMiddleware(stateManager);
    const baseAssetsUrl = vscode.Uri.joinPath(
      this.#context.extensionUri,
      './assets/language-server/',
    ).toString();

    this.#client = this.#clientFactory.createLanguageClient(this.#context, {
      documentSelector: [{ scheme: 'file' }, { scheme: 'gitlab-web-ide' }],
      initializationOptions: {
        ...getClientContext(),
        baseAssetsUrl,
      },
      middleware,
    });

    middleware.client = this.#client;
    stateManager.client = this.#client;
    await stateManager.init();

    const suggestionsManager = new GitLabPlatformManagerForCodeSuggestions(gitLabPlatformManager);
    this.#wrapper = new LanguageClientWrapper(
      this.#client,
      suggestionsManager,
      stateManager,
      gitLabTelemetryEnvironment,
    );

    await this.#wrapper.initAndStart();
    const subscriptions = [
      suggestionsManager,
      this.#wrapper,
      vscode.commands.registerCommand(
        SUGGESTION_ACCEPTED_COMMAND,
        this.#wrapper.sendSuggestionAcceptedEvent,
      ),
      vscode.commands.registerCommand(COMMAND_TOGGLE_CODE_SUGGESTIONS, () =>
        toggleCodeSuggestions({ stateManager }),
      ),
      vscode.commands.registerCommand(
        CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
        codeSuggestionStreamAccepted(this.#client),
      ),
      vscode.workspace.onDidChangeConfiguration(async e => {
        if (!e.affectsConfiguration(CONFIG_NAMESPACE)) {
          return;
        }

        await this.#wrapper?.syncConfig();
      }),
      gitLabTelemetryEnvironment.onDidChangeTelemetryEnabled(this.#wrapper.syncConfig),
      gitLabPlatformManager.onAccountChange(this.#wrapper.syncConfig),
      gitLabPlatformManager.onAccountChange(this.#wrapper.syncConfig),

      statusBarItem,
      gutterIcon,
      vscode.window.onDidChangeActiveTextEditor(async te => {
        if (te) {
          await this.#client?.sendNotification(DidChangeDocumentInActiveEditor, {
            ...te.document,
            uri: te.document.uri.toString(),
          });
        }
      }),
    ];
    this.#context.subscriptions.push(...subscriptions);
    this.#subscriptions = subscriptions;
  }

  public async restartLanguageServer() {
    if (this.#client) {
      await this.#client.stop();
      this.#client = undefined;
      this.#wrapper?.dispose();
    }
    for (const subscription of this.#subscriptions) {
      subscription.dispose();
    }
    this.#subscriptions = [];
    await this.startLanguageServer();
  }
}
