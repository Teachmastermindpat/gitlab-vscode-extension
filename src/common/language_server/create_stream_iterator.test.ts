import { BaseLanguageClient } from 'vscode-languageclient';
import { CancellationToken } from 'vscode';
import { createStreamIterator, STREAM_QUEUE_TIMEOUT_MS } from './create_stream_iterator';
import { createFakePartial } from '../test_utils/create_fake_partial';

const mockCleanupFn = jest.fn();
const mockOnNotification = jest.fn();
const mockSendNotification = jest.fn();
jest.useFakeTimers();

const streamId = 'stream-1';

const createIterator = () =>
  createStreamIterator(
    createFakePartial<BaseLanguageClient>({
      onNotification: mockOnNotification,
      sendNotification: mockSendNotification,
    }),
    streamId,
    createFakePartial<CancellationToken>({
      onCancellationRequested: () => ({
        dispose: () => {},
      }),
    }),
    mockCleanupFn,
  );

describe('Create Stream Iterator', () => {
  afterEach(() => {
    jest.clearAllTimers();
  });

  it('creates a stream iterator', () => {
    const iterator = createIterator();
    expect(iterator.next).toBeDefined();
  });

  describe('stream cleanup', () => {
    const notificationData = { id: streamId, completion: 'hello', done: false };
    let sendNotification: (_args: typeof notificationData) => void | undefined;

    beforeEach(() => {
      mockOnNotification.mockImplementation((_, _callback) => {
        sendNotification = _callback;
        return { dispose: () => {} };
      });
    });

    it('does not run cleanup when Part is requested before data is added to the queue', async () => {
      const iterator = createIterator();

      // Request the next Part
      const next = iterator.next();
      // Send new data to the iterator queue
      sendNotification(notificationData);
      await next;
      // Ensure the cleanup delay is complete
      jest.advanceTimersByTime(STREAM_QUEUE_TIMEOUT_MS);

      expect(mockCleanupFn).not.toHaveBeenCalled();
      expect(mockSendNotification).not.toHaveBeenCalled();
    });

    it('does not run cleanup when Part is requested within delay timeout', async () => {
      const iterator = createIterator();

      // Send new data to the iterator queue
      sendNotification(notificationData);
      jest.advanceTimersByTime(100);
      // Request the next Part
      await iterator.next();
      // Ensure the cleanup delay is complete
      jest.advanceTimersByTime(STREAM_QUEUE_TIMEOUT_MS);

      expect(mockCleanupFn).not.toHaveBeenCalled();
      expect(mockSendNotification).not.toHaveBeenCalled();
    });

    it('calls the cleanup function when no Part is requested', () => {
      createIterator();

      // Send new data to the iterator queue
      sendNotification(notificationData);
      expect(mockCleanupFn).not.toHaveBeenCalled();
      // Ensure the cleanup delay is complete
      jest.advanceTimersByTime(STREAM_QUEUE_TIMEOUT_MS);
      // Note the lack of requests to the iterator for the next Part

      expect(mockCleanupFn).toHaveBeenCalled();
      expect(mockSendNotification).toHaveBeenCalled();
    });

    it('calls the cleanup function if a Part is requested too late', async () => {
      const iterator = createIterator();

      // Send new data to the iterator queue
      sendNotification(notificationData);
      expect(mockCleanupFn).not.toHaveBeenCalled();
      // Ensure the cleanup delay is complete
      jest.advanceTimersByTime(STREAM_QUEUE_TIMEOUT_MS);
      // Request the next Part
      await iterator.next();

      expect(mockCleanupFn).toHaveBeenCalled();
      expect(mockSendNotification).toHaveBeenCalled();
    });
  });
  describe('queue batch handling', () => {
    const createNotificationData = (id: string, completion: string, done: boolean) => ({
      id,
      completion,
      done,
    });

    let sendNotification: (_args: {
      id: string;
      completion: string;
      done: boolean;
    }) => void | undefined;

    beforeEach(() => {
      mockOnNotification.mockImplementation((_, _callback) => {
        sendNotification = _callback;
        return { dispose: () => {} };
      });
    });

    it('returns the last completion when queue has multiple items', async () => {
      const iterator = createIterator();

      sendNotification(createNotificationData(streamId, 'hello', false));
      sendNotification(createNotificationData(streamId, 'world', false));
      sendNotification(createNotificationData(streamId, '!', false));

      const { value } = await iterator.next();
      expect(value).toEqual({ completion: '!', canceled: false });
    });

    it('returns the last completion when done is true', async () => {
      const iterator = createIterator();

      sendNotification(createNotificationData(streamId, 'hello', false));
      sendNotification(createNotificationData(streamId, 'world', false));
      sendNotification(createNotificationData(streamId, '!', true));

      const { value, done } = await iterator.next();
      expect(value).toEqual({ completion: '!', canceled: false });
      expect(done).toBe(true);
    });
  });
});
